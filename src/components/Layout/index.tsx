import Head from 'next/head';

import { ILayoutProps } from 'types';

const Layout = ({ children, title }: ILayoutProps) => {
    return (
        <>
            <Head>
                <title>{title} | Project Setup V2</title>
                <link rel="icon" href="/favicon.ico" />

                <meta charSet="utf-8" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="project setup v2 content" />
            </Head>

            <header>Header</header>

            {children}

            <footer>Footer</footer>
        </>
    );
};

export default Layout;
